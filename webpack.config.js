/* eslint-disable no-var */

var webpack = require('webpack');
var path = require('path');

module.exports = {
    default: {
        devtool: 'cheap-module-eval-source-map',
        entry: [
            'eventsource-polyfill', // necessary for hot reloading with IE
            './src/index'
        ],
        target: 'web',
        output: {
            path: __dirname + '/dist', // Note: Physical files are only output by the production build task `npm run build`.
            publicPath: '/',
            filename: 'bundle.js'
        },
        devServer: {
            contentBase: './src',
            port: 9000,
            hot: true
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoEmitOnErrorsPlugin()
        ],
        watchOptions: {
            aggregateTimeout: 300,
            ignored: /node_modules/,
            poll: 500
        },
        module: {
            rules: [
                {test: /\.js$/, include: path.join(__dirname, 'src'), loader: 'babel-loader'},
                {test: /(\.css)$/, use: ['style-loader', 'css-loader']},
                {test: /(\.scss)$/, use: ['style-loader', 'css-loader', 'sass-loader']},
                {test: /\.jsx$/, include: path.join(__dirname, 'src'), loader: 'babel-loader'}
            ]
        }
    }
};