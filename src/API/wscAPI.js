// support for API calls.
// I kept low level (generic get/post and higher level (get/save player/s) calls in the same class, in real life we may want to separate them

import 'whatwg-fetch';
import {ErrorCodes} from '../Utils/constants';
import Texts from '../Utils/texts';
import ApiError from './apiError';
import Utils from '../Utils/utils';

class WscAPI
{
    static getData(url)
    {
        // Could use input validity checks here
        return new Promise( (resolve, reject) =>
        {
            let myRequest = new Request(url, {
                method: 'GET',
                credentials: 'same-origin',
                cache: 'no-store',
                headers: {'Content-Type': "application/json"}});
            fetch(myRequest).then(response => {
                    if (response.ok) {
                        resolve(response.json());
                    }
                    else {
                        reject (new ApiError(response.statusText, response.code, ErrorCodes.ApiError ));
                    }
            }).catch( error =>
                {
                    reject(error);
                }
            );
        });

    }

    static postData(url, body)
    {
        // Could use input validity checks here on url & body
        return new Promise( (resolve, reject) =>
        {
            const bodyStr = JSON.stringify(body);
            let myRequest = new Request(url, {
                method: 'POST',
                credentials: 'same-origin',
                cache: 'no-store',
                headers: {'Content-Type': "application/json"},
                body: bodyStr});
            fetch(myRequest).then(response => {
                if (response.ok) {
                    // That's too naive, we ignore the result but for the sake of the exercise it's good enough
                    resolve(true);
                }
                else {
                    reject (new ApiError(response.statusText, response.code, ErrorCodes.ApiError ));
                }
            }
            );
        });

    }


    static getAllPlayers(url)
    {
        return new Promise ( (resolve, reject) => {
            WscAPI.getData(url).then(data =>
            {
                if (data && Array.isArray(data) && data.length > 0 && !data.find(number => !(Utils.isNumeric(number)))) {
                    resolve(data);
                }
                else
                    reject (new ApiError(Texts.AllPlayersResponseInvalid, ErrorCodes.AllPlayersResponseInvalid, ErrorCodes.AllPlayersResponseInvalid));
            }).catch(error => {reject(error);});
        });
    }

    static getOnePlayer(url)
    {
        return new Promise ( (resolve, reject) => {
            WscAPI.getData(url).then(data =>
            {
                if (data && Utils.isObject(data)) {
                    resolve(data);
                }
                else
                    reject (new ApiError(Texts.OnePlayersResponseInvalid, ErrorCodes.OnePlayersResponseInvalid, ErrorCodes.OnePlayersResponseInvalid));
            }).catch(error => {reject(error);});
        });
    }

    static save(url, info)
    {
        return WscAPI.postData(url, info);
    }
}

export default WscAPI;