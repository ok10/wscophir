// Meant to support a 'typed' API exception. Could do without it.

class apiError extends Error
{
    constructor(message, id, code)
    {
        super(message, id);
        this.errCode = code;
    }

    get message()
    {
        return this.message;
    }

    get id()
    {
        return this.id;
    }

    get code()
    {
        return this.errCode;
    }
}

export default apiError;