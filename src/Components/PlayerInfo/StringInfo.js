import PropTypes from 'prop-types';
import React from 'react';
import BaseFieldInfo from './BaseFieldInfo';

class StringInfo extends BaseFieldInfo {
    render()
    {
        return (
            <input type="text" value={this.state.value} onChange={this.onChange}/>
        );
    }
}
StringInfo.propTypes = {
    path: PropTypes.array.isRequired,
    value: PropTypes.string.isRequired
};

export default StringInfo;