import PropTypes from 'prop-types';
import React from 'react';
import Utils from '../../Utils/utils';
import BaseFieldInfo from './BaseFieldInfo';

class UrlInfo extends BaseFieldInfo {
    constructor(props)
    {
        super(props);
        this.isVideo = Utils.isVideo(this.props.value);
    }

    render()
    {
        /* media source doesn't change until we save, because we use the value from props (not from state) */
        const mediaBox = this.isVideo ? (
            <video className="info-url" controls>
                <source src={this.props.value}/>
            </video>
        ) :
            (
                <img className="info-url" src={this.props.value}/>
            );

        return (
            <div className="url-field">
                <input type="text" value={this.props.value} onChange={this.onChange}/>
                {mediaBox}
            </div>
        );
    }
}

UrlInfo.propTypes = {
    path: PropTypes.array.isRequired,
    value: PropTypes.string.isRequired,
    updateField:PropTypes.func.isRequired
};

export default UrlInfo;