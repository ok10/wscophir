import PropTypes from 'prop-types';
import React from 'react';

class BaseFieldInfo extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {value: props.value};
        this.onChange =this.onChange.bind(this);
    }

    componentWillReceiveProps(newProps)
    {
        // that should support updates by polling. Never saw it working
        if (this.props.value !== newProps.value) {
            this.setState({value: newProps.value});
        }
    }

    onChange(evt)
    {
        const value = evt.target.value;
        this.setState({value});
        this.props.updateField(this.props.path, value);
    }

    // Never called...
    render()
    {
        return (
            <div/>
        );
    }
}
BaseFieldInfo.propTypes = {
    path: PropTypes.array.isRequired,
    value: PropTypes.number.isRequired,
    updateField: PropTypes.func.isRequired
};

export default BaseFieldInfo;