import PropTypes from 'prop-types';
import React from 'react';
const InfoField=({path, fieldName, level, children}) =>
{
    function onChange()
    {
    }

    return (
        // I thought static level styling is a better solution. We could also dynamically calculate the style (margin and square color)

        <div className={"info-field level" + level.toString()}>
            <span className="square"/>
            {fieldName}:
            {children}
        </div>
    );
};

InfoField.propTypes = {
    path: PropTypes.array.isRequired,
    fieldName: PropTypes.string.isRequired,
    level: PropTypes.number.isRequired,
    children: PropTypes.object.isRequired
};

export default InfoField;