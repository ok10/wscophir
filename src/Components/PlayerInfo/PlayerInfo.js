/**
 * Created by Ophir.Kenig on 19/05/2017.
 */
import React from 'react';
import Utils from '../../Utils/utils';
import PropTypes from 'prop-types';
import NumericInfo from './NumericInfo';
import UrlInfo from './UrlInfo';
import StringInfo from './StringInfo';
import Texts from '../../Utils/texts';
import InfoField from './InfoField';
import './PlayerInfo.scss';

const PlayerInfo = ({playerInfo, path, level, updateField}) => {
    const infoRows = [];

    if (Utils.isObject(playerInfo))
    {
        for (let key in playerInfo)
        {
            const currentPath = path.concat([key]);
            const value = playerInfo[key];

            if (Utils.isObject(value) && value)
                infoRows.push (
                    <InfoField path={currentPath} fieldName={key} level={level} key={currentPath}>
                        <PlayerInfo
                            playerInfo={value}
                            path={currentPath}
                            level={level + 1}
                            updateField={updateField}
                        />
                    </InfoField>
                );
            else if (Utils.isNumeric(value))
                infoRows.push (
                    <InfoField path={currentPath} fieldName={key} level={level} key={currentPath}>
                        <NumericInfo
                            value={value}
                            path={currentPath}
                            updateField={updateField}
                        />
                    </InfoField>
                );
            else if (Utils.isUrl(value))
                infoRows.push (
                    <InfoField path={currentPath} fieldName={key} level={level} key={currentPath}>
                          <UrlInfo
                            value={value}
                            path={currentPath}
                            updateField={updateField}
                            />
                    </InfoField>
                );
            else if (Utils.isText(value))
                infoRows.push (
                    <InfoField path={currentPath} fieldName={key} level={level} key={currentPath}>
                        <StringInfo
                            value={value}
                            path={currentPath}
                            updateField={updateField}
                        />
                    </InfoField>
                );
        }
        return (
            <div className={"player-info level" + level.toString()}>
                {infoRows}
            </div>

        );
    }
    else
        // A bit naive but should occur only on first level
        return (
            <div>
                {Texts.Loading}
            </div>
        );


};

PlayerInfo.propTypes = {
    path: PropTypes.array.isRequired,
    level: PropTypes.number.isRequired,
    playerInfo: PropTypes.object,
    updateField: PropTypes.func.isRequired
};
export default PlayerInfo;