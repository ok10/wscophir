import PropTypes from 'prop-types';
import React from 'react';
import BaseFieldInfo from './BaseFieldInfo';

class NumericInfo extends BaseFieldInfo {
    render()
    {

        return (
            <input type="number" value={this.state.value} onChange={this.onChange}/>
        );
    }
}
NumericInfo.propTypes = {
    path: PropTypes.array.isRequired,
    value: PropTypes.number.isRequired,
    updateField: PropTypes.func.isRequired
};

export default NumericInfo;