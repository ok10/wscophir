/**
 * Created by Ophir.Kenig on 19/05/2017.
 */
import React from 'react';
import ReactDom from 'react-dom';
import WscAPI from '../../API/wscAPI';
import Texts from '../../Utils/texts';
import PropTypes from 'prop-types';
import PlayerInfo from '../PlayerInfo/PlayerInfo';
import Utils from '../../Utils/utils';
import './MainPanel.scss';


class MainPanel extends React.Component
{
    constructor(props)
    {
        super(props);
        const {allPlayersUrl, playerUrlTemplate} = props;
        this.allPlayersUrl = allPlayersUrl;
        this.playerUrlTemplate = playerUrlTemplate;
        this.state = {currentPlayer: null};
        this.failurePollTime = 5000;
        this.successPollTime = 1000;
        this.getAllPlayers = this.getAllPlayers.bind(this);
        this.getOnePlayer = this.getOnePlayer.bind(this);
        this.updateField = this.updateField.bind(this);
        this.save = this.save.bind(this);
        this.updates = {};
    }

    componentDidMount()
    {
        this.getAllPlayers();
    }

    // In a more complex application we'd probably want to use a state management tool to handle the API flows, so calls and result are tracked by relevant components
    getAllPlayers()
    {
        WscAPI.getAllPlayers(this.allPlayersUrl).then( allPlayers => {
           this.getOnePlayer(allPlayers[0]);
           })
           .catch(errorResult => {
               this.setState( {errorMessage: (errorResult && errorResult.message)? errorResult.message : Texts.UnknownError});
               setTimeout(this.getAllPlayers, this.failurePollTime);
           });

    }

    pollPlayer(timeout, playerId)
    {
        const that = this;
        clearTimeout(this.playerPollHandle);
       this.playerPollHandle = setTimeout(function () {
            that.getOnePlayer(playerId || that.state.currentPlayer);
        }, timeout);
    }

    // In a more complex application we'd probably want to use a state management tool to handle the API flows, so calls and result are tracked by relevant components
    getOnePlayer(playerId){
        this.currentPlayerUrl = Utils.stringFormat(this.playerUrlTemplate, playerId);
        const that = this;
        WscAPI.getOnePlayer(this.currentPlayerUrl).then (playerInfo => {
            this.setState( {playerInfo, currentPlayer: playerId, errorMessage: null /* This may clear the error too soon */});
            this.pollPlayer(this.successPollTime, playerId);
        }).catch(error =>
            {
                that.setState ({errorMessage: error.message});
                that.pollPlayer(that.failurePollTime, playerId);

            }
        );
    }

    // In a more complex application we'd probably want to involve a state management tool to track the updates
    updateField(path, value)
    {
        let currentObj = this.updates;
        const last = path.length - 1;
        for (let i=0; i< last; i++)
        {
            if (!currentObj[path[i]])
                currentObj[path[i]] = {};
            currentObj = currentObj[path[i]];
        }
        currentObj[path[last]] = value;
    }

    save()
    {
        // should probably disable the 'Save' button if no updates
        if (this.updates) {
            const that = this;
            clearTimeout(this.playerPollHandle);
            WscAPI.save(this.currentPlayerUrl, this.updates).then(response =>
            {
                let newInfo = Object.assign({}, that.state.currentPlayer, that.updates);
                that.setState({playerInfo: newInfo});
                that.updates = {};
                that.pollPlayer(that.successPollTime);
            })
                .catch(error =>
                {
                    that.setState({errorMessage: Texts.SaveError + error.message});
                    that.pollPlayer(that.failurePollTime);
                });
        }

    }

    render()
    {
        const errDiv = this.state.errorMessage ? (<div className="err-msg">{this.state.errorMessage}</div>) : null;
        const playerInfoDiv = this.state.playerInfo ? (
            <PlayerInfo path={[]} level={0} playerInfo={this.state.playerInfo} updateField={this.updateField}/>
        ) : null;

        // May want to disable button when no updates
        const saveButton =  this.state.playerInfo ? (<button className="save-button" onClick={this.save}>{Texts.Save}</button>) : null;
        return (
            <div className="main-panel">
                <div className="main-panel-info">
                    {playerInfoDiv}
                </div>
                {errDiv}
                {saveButton}
            </div>
        );
    }
}

MainPanel.propTypes = {
    allPlayersUrl: PropTypes.string.isRequired,
    playerUrlTemplate: PropTypes.string.isRequired
};

export default MainPanel;