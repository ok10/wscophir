export default class Utils
{
    static isObject(obj) {
        return obj === Object(obj);
    }

    static isNumeric(value)
    {
        return !isNaN(value);
    }

    static isUrl(value)
    {
        return Utils.isText(value) && value.match(Utils.UrlRegex);
    }

    static isText(value)
    {
        return typeof (value) == 'string';
    }

    // Checking if a url represents a video by the file extension may not be optimal, but as long as we have it as a service function we could improve it without affecting the rest
    static isVideo(url)
    {
        if (!url)
            return false;
        const dotPos = url.lastIndexOf('.');
        if (dotPos > -1)
        {
            const extension = url.substr(dotPos+1).toLowerCase();
            return !!Utils.VideoExtensions.find( val => val == extension);
        }
        return false;
    }

    // A very naive one. We can of course more sophisticated open source tools for that, but we don't really need it for the exercise
    static stringFormat(template)
    {
        return template.replace('{0}', arguments[1]);
    }
}

Utils.UrlRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
Utils.VideoExtensions = ['mpg', 'mp4', 'avi'];  // need to add others...
