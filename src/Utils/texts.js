export const Texts_EN =
{
    ApiError: "An error occurred",
    AllPlayersResponseInvalid: "Response for players list was invalid",
    OnePlayersResponseInvalid: "Response for one player was invalid",
    UnknownError: "An unknown error has occurred",
    Loading: "Loading...",
    Save: "Save",
    SaveError: "Error on save: "
};

export default Texts_EN;

