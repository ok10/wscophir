import React from 'react';
import ReactDOM from 'react-dom';
import Promise from 'promise-polyfill';
import MainPanel from './Components/MainPanel/MainPanel';

// To add to window
if (!window.Promise) {
    window.Promise = Promise;
}
const server = "https://nodesenior.azurewebsites.net";
const allPlayersUrl = server + "/player/all";
const playerUrlTemplate = server + "/player/{0}";

const main_layout = (
        <div>
            <MainPanel allPlayersUrl={allPlayersUrl} playerUrlTemplate={playerUrlTemplate}/>
        </div>
);

ReactDOM.render(main_layout, document.getElementById('root'));
