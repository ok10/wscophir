# WSC-SPORTS Senior Developer Exercise #
##Player info / edit ##
####By Ophir Kenig

*Version 1.0*

###Setup:
* Make sure you have node.js installed.
* Copy everything to the home directory.
* run 'npm install'
* run 'npm start'

__Build and run notes:__

* No unit tests were written, this seemed out of scope of the exercise.
* There are only development mode npm scripts, project needs to be run on development server.

__Contact me:__
If you have questions, please call me (054-3266565) or email (ophir.kenig@gmail.com)